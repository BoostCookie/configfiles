#!/usr/bin/env python3

import os
import hashlib
import colorsys
import socket

# minimum brightness
min_light: float = 0.6

def name_to_hl(name: str) -> tuple[float, float]:
    hex_str = hashlib.md5(name.encode(), usedforsecurity=False).hexdigest()
    a = int(hex_str[:4], base=16)
    b = int(hex_str[4:8], base=16)
    return (a / 0xffff, max(min_light, b / 0xffff))

# converts hue and light to rgb
def hl_to_rgb(h: float, l: float) -> tuple[int, int, int]:
    r, g, b = colorsys.hls_to_rgb(h, l, 1.0)
    return int(255 * r), int(255 * g), int(255 * b)

def name_to_rgb(name: str) -> tuple[int, int, int]:
    return hl_to_rgb(*name_to_hl(name))

def rgb_to_hex(rgb: tuple[int, int, int]) -> str:
    return format(rgb[0], "X") + format(rgb[1], "X") + format(rgb[2], "X")

def hl_to_hex(h: float, l: float) -> str:
    return rgb_to_hex(hl_to_rgb(h, l))

# adds two hues on a circle that has already two hues on them;
# in the end all hues are as equally spaced as possible
def add_two_distant_hues(hue1: float, hue2:float) -> tuple[float, float]:
    dist = abs(hue1 - hue2)
    mean_hue = (hue1 + hue2) / 2
    opposite_mean_hue = (mean_hue + 0.5) % 1.0
    if 0.4 < dist and dist < 0.6:
        # in the middle and opposite of it
        return mean_hue, opposite_mean_hue
    else:
        # evenly spaced on the other side
        middle_hue = opposite_mean_hue
        if dist > 0.5:
            middle_hue = mean_hue
        else:
            dist = 1.0 - dist
        offset = dist / 6.0
        return (middle_hue + offset) % 1.0, (middle_hue - offset) % 1.0

def prompt_colours(user: str, host: str) -> tuple[tuple[int, int, int], tuple[int, int, int], tuple[int, int, int], tuple[int, int, int]]:
    user_h, user_l = name_to_hl(user)
    host_h, host_l = name_to_hl(host)
    bracket_h, at_h = add_two_distant_hues(user_h, host_h)
    bracket_l, at_l = min_light, min_light
    return hl_to_rgb(user_h, user_l), hl_to_rgb(host_h, host_l), hl_to_rgb(bracket_h, bracket_l), hl_to_rgb(at_h, at_l)

def main():
    user_rgb, host_rgb, bracket_rgb, at_rgb = prompt_colours(os.environ["USER"], socket.gethostname())

    userprompt = "%F{#" + rgb_to_hex(user_rgb) + "}%n"
    hostprompt = "%F{#" + rgb_to_hex(host_rgb) + "}%m"
    bracket_hex = rgb_to_hex(bracket_rgb)
    at_hex = rgb_to_hex(at_rgb)
    print("%F{#" + bracket_hex + "}[%(!.%F{#ff0000}%B%n%b." + userprompt + ")%F{#" + at_hex + "}@" + hostprompt + "%f %1~%F{#" + bracket_hex + "}]%f%(!.%B.)%#%b ")

if __name__ == "__main__":
    main()
