#!/usr/bin/env python3

import colorsys
from sty import fg
import numpy as np
from prompt import name_to_rgb, name_to_hl, add_two_distant_hues, prompt_colours
import matplotlib.pyplot as plt
import numpy as np
from math import tau
import random

def main():
    sentence = "geher geher-pc geher-laptop gsuser gserver horizon apex netzteam firewall \n This is a rather fun way of colouring text. Same words have the same colour. This is true! It is case sensitive though."
    for word in sentence.split(" "):
        print(fg(*name_to_rgb(word)) + word + fg.rs, end=" ")

    print()
    for hue in np.arange(0, 1.0, 0.05):
        print(fg(*[int(255 * c) for c in colorsys.hls_to_rgb(hue, 0.6, 1.0)]) + str(round(hue, 2)) + fg.rs, end=" ")

    print("\ngeher")
    print(name_to_hl("geher"))
    print("geher-pc")
    print(name_to_hl("geher-pc"))
    print("rather")
    print(name_to_hl("rather"))
    print(f"{add_two_distant_hues(0.1, 0.9)=}")
    print(f"{add_two_distant_hues(0.1, 0.4)=}")
    print(f"{add_two_distant_hues(0.02, 0.65)=}")
    print(f"{add_two_distant_hues(0.65, 0.02)=}")


    prompts = [("geher", "geher-laptop"), ("user", "host"), ("dude", "computer"), ("otherdude", "computer"), ("dude", "bhost")]
    for user, host in prompts:
        user_rgb, host_rgb, bracket_rgb, at_rgb = prompt_colours(user, host)
        print(fg(*bracket_rgb)+  "[" + fg(*user_rgb) + user + fg(*at_rgb) + "@" + fg(*host_rgb) + host + fg.rs + " ~" + fg(*bracket_rgb) + "]" + fg.rs + "% ")

    random.seed()
    fig, axes = plt.subplots(nrows=2, ncols=2, layout="constrained")
    for i, ax in enumerate(axes.flatten()):
        ax.set_aspect("equal")
        phirange = np.arange(0, tau, 0.01)
        ax.plot(np.cos(phirange), np.sin(phirange), "-", color="black")
        hue1 = random.random()
        hue2 = random.random()
        if i == 0:
            hue1 = 0.52
            hue2 = 0.42
        hue3, hue4 = add_two_distant_hues(hue1, hue2)
        ax.plot(np.cos(hue1 * tau), np.sin(hue1 * tau), "x", color="blue")
        ax.plot(np.cos(hue2 * tau), np.sin(hue2 * tau), "x", color="blue")
        ax.plot(np.cos(hue3 * tau), np.sin(hue3 * tau), "x", color="red")
        ax.plot(np.cos(hue4 * tau), np.sin(hue4 * tau), "x", color="red")

        ax.text(np.cos(hue1 * tau), np.sin(hue1 * tau), "hue1: {}".format(round(hue1, 2)))
        ax.text(np.cos(hue2 * tau), np.sin(hue2 * tau), "hue2: {}".format(round(hue2, 2)))
        ax.text(np.cos(hue3 * tau), np.sin(hue3 * tau), "hue3: {}".format(round(hue3, 2)))
        ax.text(np.cos(hue4 * tau), np.sin(hue4 * tau), "hue4: {}".format(round(hue4, 2)))

        hues = [hue1, hue2, hue3, hue4]
        hues.sort()
        for hue_low, hue_high in zip(hues, hues[1:]):
            dist = hue_high - hue_low
            phi = (hue_low + dist / 2) * tau
            ax.text(np.cos(phi), np.sin(phi), "{}".format(round(dist, 2)))
        # last distance
        dist = 1.0 - (hues[-1] - hues[0])
        phi = ((hues[-1] + dist / 2) % 1.0) * tau
        ax.text(np.cos(phi), np.sin(phi), "{}".format(round(dist, 2)))
    fig.savefig("fig.pdf") # type: ignore

if __name__ == "__main__":
    main()
