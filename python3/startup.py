import atexit
import os
import readline

histfile = os.path.join(os.path.expanduser("~"), ".python_history")
if "PYTHONHISTFILE" in os.environ:
    histfile = os.path.expanduser(os.environ["PYTHONHISTFILE"])
elif "XDG_STATE_HOME" in os.environ:
    histfile = os.path.join(os.path.expanduser(os.environ["XDG_STATE_HOME"]), "python", "python_history")

histfile = os.path.abspath(histfile)
histdir = os.path.dirname(histfile)
os.makedirs(histdir, exist_ok=True)

try:
    readline.read_history_file(histfile)
    # default history len is -1 (infinite), which may grow unruly
    readline.set_history_length(1000)
except FileNotFoundError:
    pass

atexit.register(readline.write_history_file, histfile)
