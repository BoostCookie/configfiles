" spell checking for English and German
setlocal spell
set spelllang=en_gb,de_20
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u
