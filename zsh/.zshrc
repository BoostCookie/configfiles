HISTSIZE=1000
SAVEHIST=1000
unsetopt autocd
bindkey -v

# load aliases
[ -f "$HOME/.config/aliases_env/aliases" ] && source "$HOME/.config/aliases_env/aliases"

fpath=(~/.config/zsh/myfunctions $fpath)
# termux
[ -n "$PREFIX" ] && fpath=($PREFIX/local/share/zsh/site-functions $fpath)
#fpath=(/tmp/complete $fpath)

autoload -U colors && colors

# prompt
autoload -Uz promptinit
promptinit
prompt namedependent

# === completion ===
zmodload zsh/complist
zstyle :compinstall filename "$HOME/.config/zsh/.zshrc"
_comp_options+=(globdots)

setopt AUTO_LIST            # Automatically list choices on ambiguous completion
setopt AUTO_PARAM_SLASH     # Complete directories with a trailing /
setopt CHASE_LINKS          # Expand symbolic links and .. (implicit CHASE_DOTS)
setopt COMPLETE_IN_WORD     # Start completion on both ends
setopt INTERACTIVECOMMENTS  # Comments in shell
setopt LIST_AMBIGUOUS       # Automatically highlight first element of completion menu
setopt LIST_PACKED          # Denser menu but different column width
setopt LIST_TYPES
setopt SHARE_HISTORY

zstyle ':completion:*' completer _extensions _complete _approximate
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh/zcompcache

zstyle ':completion:*' menu select
zstyle ':completion::complete:*' gain-privileges 1

zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (errors: %e) -!%f'
#zstyle ':completion:*:*:*:*:descriptions' format '%F{blue}-- %D %d --%f'
zstyle ':completion:*:*:*:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:*:*:*:warnings' format ' %F{red}-- no matches found --%f'
# enable color
zstyle ':completion:*:*:*:*:default' list-colors ''
zstyle ':completion:*' list-colors ''

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' keep-prefix true
zstyle -e ':completion:*:(ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'


# === keybinds ===
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

typeset -g -A key
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

bindkey -- "^a"                  beginning-of-line
bindkey -- "^e"                  end-of-line
bindkey -- "^r"                  history-incremental-pattern-search-backward
bindkey -- "^[[1;5C"             forward-word
bindkey -- "^[[1;5D"             backward-word
bindkey -- "${key[PageUp]}"      beginning-of-buffer-or-history
bindkey -- "${key[PageDown]}"    end-of-buffer-or-history
bindkey -- "${key[Shift-Tab]}"   reverse-menu-complete

bindkey -M menuselect 'h'                vi-backward-char
bindkey -M menuselect 'k'                vi-up-line-or-history
bindkey -M menuselect 'j'                vi-down-line-or-history
bindkey -M menuselect 'l'                vi-forward-char
bindkey -M menuselect '^d'               vi-forward-word
bindkey -M menuselect '^u'               vi-backward-word
bindkey -v '^?' backward-delete-char

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

autoload -Uz compinit
compinit -i -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION

# Syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

# for termux
if [ -n "$XDG_RUNTIME_DIR" ]; then
	mkdir -p $XDG_RUNTIME_DIR

	if ! pgrep -u "$USER" ssh-agent > /dev/null; then
		ssh-agent -t 168h > "$XDG_RUNTIME_DIR/ssh-agent.env"
	fi
	if [ -f "$XDG_RUNTIME_DIR/ssh-agent.env" ]; then
		source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
	fi
fi
